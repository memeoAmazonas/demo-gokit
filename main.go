package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/memeoAmazonas/demo-2/common"
	"github.com/memeoAmazonas/demo-2/database"
	"github.com/memeoAmazonas/demo-2/services/appointment"

	"github.com/memeoAmazonas/demo-2/services/user"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var httpAddr = flag.String("http", ":8888", "http listen address")
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"main", "caller",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "main started")
	defer level.Info(logger).Log("message", "main end")
	ctx := context.Background()
	var db *mongo.Database
	{
		var err error
		db, err = database.GetDB()
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
	}

	flag.Parse()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	mux := http.NewServeMux()
	mux.Handle("/api/users/", user.NewHttpServer(ctx, user.UserServer(db, logger)))
	mux.Handle("/api/appointment/", appointment.NewHttpServer(ctx, appointment.AppointmentServer(db, logger)))
	http.Handle("/", common.CommonMiddleware(mux))
	go func() {
		fmt.Println("listen on port", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()
	level.Error(logger).Log("exit", <-errs)
}
