package user

import (
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/mongo"
)

func UserServer(db *mongo.Database, logger log.Logger) Endpoint {
	{
		logger = log.With(logger,
			"service", "user",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "service started")
	var srv Service
	{
		userCollection := db.Collection("user")
		repository := NewRepository(userCollection, logger)
		srv = NewService(repository, logger)
	}
	return MakeEndpoints(srv)
}
