package user

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/memeoAmazonas/demo-2/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestCreateUser(t *testing.T) {
	user := map[string]string{
		"name":     "test service",
		"email":    "test@test.com",
		"password": "password",
	}
	t.Run("Passing all params", func(t *testing.T) {
		srv, ctx := Setup()
		got, _ := srv.CreateUser(ctx, user["name"], user["email"], user["password"])
		want := "user created successfully"
		assertString(t, got, want)
	})
	t.Run("missing name", func(t *testing.T) {
		srv, ctx := Setup()
		_, err := srv.CreateUser(ctx, "", user["email"], user["password"])
		want := "Name is mandatory"
		if err == nil {
			t.Fatal("expected to get an error.")
		}
		assertString(t, err.Error(), want)
	})
	t.Run("missing email", func(t *testing.T) {
		srv, ctx := Setup()
		_, err := srv.CreateUser(ctx, user["name"], "", user["password"])
		want := "Email is mandatory"
		if err == nil {
			t.Fatal("expected to get an error.")
		}
		assertString(t, err.Error(), want)
	})
	t.Run("missing password", func(t *testing.T) {
		srv, ctx := Setup()
		_, err := srv.CreateUser(ctx, user["name"], user["email"], "")
		want := "Password is mandatory"
		if err == nil {
			t.Fatal("expected to get an error.")
		}
		assertString(t, err.Error(), want)
	})
}

func TestGETUsers(t *testing.T) {

	t.Run("GET all User", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		id := addUser(ctx)
		got, _ := srv.GetAll(ctx)
		want := []User{
			{
				ID:       id.InsertedID.(primitive.ObjectID),
				Name:     "adding 1",
				Email:    "adding@test.com",
				Password: "adddingpassword"},
		}
		assertList(t, got, want)
	})
	t.Run("GET all User from clear table", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		_, err := srv.GetAll(ctx)
		want := ErrEmpty.Error()
		assertString(t, err.Error(), want)
	})
}
func TestGetUser(t *testing.T) {
	t.Run("GET User by ID", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		id := addUser(ctx)
		idstr := id.InsertedID.(primitive.ObjectID).String()
		got, _ := srv.GetById(ctx, idstr[10:len(idstr)-2])
		want := User{

			ID:       id.InsertedID.(primitive.ObjectID),
			Name:     "adding 1",
			Email:    "adding@test.com",
			Password: "adddingpassword",
		}
		assertUser(t, got, want)
	})
	t.Run("GET User by  wrong ID", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		id := addUser(ctx)
		idstr := id.InsertedID.(primitive.ObjectID).String()
		_, err := srv.GetById(ctx, idstr[10:len(idstr)-1])
		want := err.Error()
		assertString(t, err.Error(), want)
	})
}
func TestUpdateUser(t *testing.T) {

}
func TestDeleteUser(t *testing.T) {
	t.Run("Delete User by ID", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		id := addUser(ctx)
		idstr := id.InsertedID.(primitive.ObjectID).String()
		got, _ := srv.Delete(ctx, idstr[10:len(idstr)-2])
		want := "user delete successfully"
		assertString(t, got, want)
	})
	t.Run("Delete User by wrong ID", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		id := addUser(ctx)
		idstr := id.InsertedID.(primitive.ObjectID).String()
		_, err := srv.Delete(ctx, idstr[10:len(idstr)-1])
		want := "the user does not exist"
		assertString(t, err.Error(), want)
	})
	t.Run("Delete User by ID on empty table", func(t *testing.T) {
		srv, ctx := Setup()
		clearBD(ctx)
		_, err := srv.Delete(ctx, "")
		want := "the user does not exist"
		assertString(t, err.Error(), want)
	})
}

func assertString(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got: %s want: %s", got, want)
	}
}

func assertList(t testing.TB, got, want []User) {
	t.Helper()
	if len(got) != len(want) {
		t.Errorf("got: %s want: %s", got, want)
	}
	if got[0] != want[0] {
		t.Errorf("got: %s want: %s", got, want)
	}

}

func assertUser(t testing.TB, got, want User) {
	if got != want {
		t.Errorf("got: %s want: %s", got, want)
	}
}

func Setup() (srv Service, ctx context.Context) {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "test service",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "test service started")
	defer level.Info(logger).Log("message", "test service end")
	db := connectDB()
	userCollection := db.Collection("user")
	repository := NewRepository(userCollection, logger)

	return NewService(repository, logger), context.Background()
}

func clearBD(ctx context.Context) {
	db := connectDB()
	userCollection := db.Collection("user")
	result, _ := userCollection.DeleteMany(ctx, bson.M{})
	fmt.Printf("DeleteMany removed %v document(s)\n", result)
}

func addUser(ctx context.Context) *mongo.InsertOneResult {
	db := connectDB()
	userCollection := db.Collection("user")
	result, _ := userCollection.InsertOne(ctx, bson.M{"name": "adding 1", "email": "adding@test.com", "password": "adddingpassword"})
	return result
}

func connectDB() *mongo.Database {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"service", "db test connect",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}
	level.Info(logger).Log("message", "connected testdb")
	defer level.Info(logger).Log("message", "Disconnected testdb")
	var db *mongo.Database
	{
		var err error
		db, err = database.GetDB()
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}

	}
	return db
}

//*IMPORTANTE NO CORRER LA PRUEBA SI EL SERVIDOR ESTA FUNCIONANDO
