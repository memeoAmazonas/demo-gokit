package appointment

import (
	"context"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func NewHttpServer(_ context.Context, endpoint Endpoint) http.Handler {

	r := mux.NewRouter()

	appointmentRoute := r.PathPrefix("/api/appointment").Subrouter()

	appointmentRoute.Methods("POST").Path("/new").Handler(httptransport.NewServer(
		endpoint.Create,
		DecodeAppointmentReq,
		EncodeResponse))

	appointmentRoute.Methods("PUT").Path("/update/{id}").Handler(httptransport.NewServer(
		endpoint.Update,
		DecodeId,
		EncodeResponse))
	appointmentRoute.Methods("GET").Path("/{id}").Handler(httptransport.NewServer(
		endpoint.GetById,
		DecodeId,
		EncodeResponse))
	return appointmentRoute
}
