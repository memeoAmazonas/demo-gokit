package appointment

import "context"

type Service interface {
	Create(ctx context.Context, id string, isType string) (string, error)
	Update(ctx context.Context, id string) (string, error)
	GetById(ctx context.Context, id string) (AppointmentUser, error)
}
