package appointment

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	Create  endpoint.Endpoint
	GetById endpoint.Endpoint
	Update  endpoint.Endpoint
}

func MakeEndPoints(s Service) Endpoint {
	return Endpoint{
		Create:  makeCreateAppointment(s),
		GetById: makeGetByIdAppointment(s),
		Update:  makeUpdateAppointment(s),
	}
}

func makeCreateAppointment(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AppointmentRequest)
		msg, err := s.Create(ctx, req.IdUser, req.Type)
		return AppointmentResponse{
			Message: msg,
		}, err
	}
}
func makeUpdateAppointment(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AppointmentRequest)
		msg, err := s.Update(ctx, req.ID)
		return AppointmentResponse{
			Message: msg,
		}, err
	}
}

func makeGetByIdAppointment(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(AppointmentRequest)
		app, err := s.GetById(ctx, req.ID)
		return AppointmentResponse{
			Message:     "Appointment info",
			Appointment: &app,
		}, err
	}
}
