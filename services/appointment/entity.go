package appointment

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"net/http"
)

type AppointmentRequest struct {
	ID     string `json:"id,omitempty"`
	Type   string `json:"type,omitempty"`
	IdUser string `json:"idUser,omitempty"`
}

type AppointmentResponse struct {
	Message         string            `json:"message"`
	AppointmentList []AppointmentUser `json:"appointment_list,omitempty"`
	Appointment     *AppointmentUser  `json:"appointment,omitempty"`
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func DecodeAppointmentReq(_ context.Context, r *http.Request) (interface{}, error) {
	var req AppointmentRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}
func DecodeId(_ context.Context, r *http.Request) (interface{}, error) {
	var req AppointmentRequest
	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		return nil, errors.New("bad request id is required")
	}
	req.ID = id
	return req, nil
}
