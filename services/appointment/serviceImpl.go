package appointment

import (
	"context"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type service struct {
	repository Repository
	logger     log.Logger
}

func NewService(repository Repository, logger log.Logger) Service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

func (s service) Create(ctx context.Context, id string, isType string) (string, error) {
	logger := log.With(s.logger, "method", "CreateAppointment")
	idUser, _ := primitive.ObjectIDFromHex(id)
	appointment := Appointment{
		ID:      primitive.NewObjectID(),
		IdUser:  idUser,
		Created: time.Now(),
		Type:    isType,
	}
	if err := s.repository.Create(ctx, appointment); err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	logger.Log("Create", "appointment success")
	return "appointment crete successfuly", nil
}

func (s service) Update(ctx context.Context, id string) (string, error) {
	logger := log.With(s.logger, "method", "UpdateAppointment")
	if err := s.repository.Update(ctx, id); err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	logger.Log("Update", "appointment success")
	return "appointment update success", nil
}

func (s service) GetById(ctx context.Context, id string) (AppointmentUser, error) {
	logger := log.With(s.logger, "method", "GetByIdAppointment")
	resp, err := s.repository.GetById(ctx, id)
	if err != nil {
		level.Error(logger).Log("err", err)
		return AppointmentUser{}, err
	}
	logger.Log("Update", "appointment success")
	return resp, nil
}
