package appointment

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Appointment struct {
	ID      primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	IdUser  primitive.ObjectID `json:"id_user"`
	Type    string             `json:"description"`
	Created time.Time          `json:"created"`
	Updated time.Time          `json:"updated,omitempty"`
}

type User struct {
	ID       primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Email    string             `json:"email"`
	Name     string             `json:"name"`
	Password string             `json:"password"`
}

type AppointmentUser struct {
	ID      primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	User    User               `json:"user"`
	Type    string             `json:"description"`
	Created time.Time          `json:"created"`
	Updated time.Time          `json:"updated,omitempty"`
}

type Repository interface {
	Create(ctx context.Context, appointment Appointment) error
	Update(ctx context.Context, id string) error
	GetById(ctx context.Context, id string) (AppointmentUser, error)
}
