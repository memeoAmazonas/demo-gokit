package appointment

import (
	"context"
	"errors"
	"github.com/go-kit/kit/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type repository struct {
	db     *mongo.Database
	logger log.Logger
}

const COLLECTION = "appointment"

func NewRepository(db *mongo.Database, logger log.Logger) Repository {
	return &repository{
		db:     db,
		logger: log.With(logger, "respository", "sql appointment"),
	}
}
func (repository *repository) Create(ctx context.Context, appointment Appointment) error {
	if err := repository.appointmentIsValid(ctx, appointment); err != nil {
		return err
	}
	_, err := repository.db.Collection(COLLECTION).InsertOne(ctx, appointment)
	if err != nil {
		return err
	}
	return nil
}

func (repository *repository) appointmentIsValid(ctx context.Context, appointment Appointment) error {
	if appointment.Type == "" {
		return errors.New("type is mandatory")
	}
	if err := repository.existUser(ctx, appointment.IdUser); err != nil {
		return err
	}
	return nil
}

func (repository *repository) existUser(ctx context.Context, id primitive.ObjectID) error {
	res := repository.db.Collection("user").FindOne(
		ctx,
		bson.M{"_id": id},
	)
	if res.Err() != nil {
		return res.Err()
	}
	return nil
}

func (repository *repository) Update(ctx context.Context, id string) error {
	docID, _ := primitive.ObjectIDFromHex(id)
	_, err := repository.db.Collection(COLLECTION).UpdateOne(
		ctx,
		bson.M{"_id": docID},
		bson.M{
			"$set": bson.M{"updated": time.Now()},
		},
	)
	return err
}
func (repository repository) GetById(ctx context.Context, id string) (AppointmentUser, error) {
	docID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": docID}
	app := Appointment{}
	user := User{}
	var response AppointmentUser
	err := repository.db.Collection(COLLECTION).FindOne(ctx, filter).Decode(&app)
	if err != nil {
		return AppointmentUser{}, err
	}
	filter = bson.M{"_id": app.IdUser}
	err = repository.db.Collection("user").FindOne(ctx, filter).Decode(&user)
	if err != nil {
		return AppointmentUser{}, err
	}
	response.ID = app.ID
	response.Type = app.Type
	response.Created = app.Created
	response.Updated = app.Updated
	response.User = user
	return response, nil
}
